const mongoose=require('mongoose')

const categorySchema=mongoose.Schema({
    name:{type:String, required:true},
    Slug:{type:String, slug:'cat_Slug'},
    date:{type:Date, default:Date.now()}
})

module.exports=mongoose.model('Category', categorySchema)