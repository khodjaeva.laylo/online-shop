const mongoose = require('mongoose');
const Region=require('./Region')
const Schema = mongoose.Schema;
const bcrypt=require('bcrypt')
const userSchema = Schema({
    email: {type:String, required: true},
    password: {type:String, required: true},
    name: {type:String, required: true},
    phone:{type:String, required:true},
    role: {type: String, enum:['admin','moderator', 'user'], default:"user"},
    regionId:{
        type:mongoose.Schema.ObjectId,
        ref:'Region'
    },
    date: {type:Date, default:Date.now()}
});
// Match user entered password to hashed password in database
userSchema.methods.matchPassword = async function(enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.password);
};

module.exports = mongoose.model('User', userSchema);