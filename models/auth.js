const mongoose = require('mongoose');
const Region=require('./Region')
const Schema = mongoose.Schema;
const authSchema = Schema({
    email: {type:String, required: true},
    password: {type:String, required: true},
    username: {type:String, required: true},
    role: {type: String, enum:['admin','moderator', 'user'], default:"user"},
    // regionId:{
    //     type:mongoose.Schema.ObjectId,
    //     ref:'Region'
    // },
    date: {type: Date, required: true}
});

module.exports = mongoose.model('Auth',authSchema)