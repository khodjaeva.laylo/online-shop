const User = require('../models/auth');
const config = require ('../config/config');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const crypto = require('crypto');

exports.register = async  ( req, res )=>{
    let authData = req.body;
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(authData.password, salt);

    let user = new User({
        username: authData.username,
        email: authData.email,
        password: password,
        date: Date.now()
    });
    user.save((error, registedAuth)=>{
        if(error){
            throw error
        }else{
            let payload = {subject: registedAuth._id};
            let token = jwt.sign(payload, config.JWT_SECRET);
            res.render('../views/admin/adlogin', {
                title:"login page",
                layout:'../views/admin/adlogin'
            })
        }
    })
};





exports.login = async (req, res) => {
    await User.findOne({ email: req.body.email }, (error, user) => {
        if (error) {
            res.send(error)
        } else {
            if (!user) {
                res.render('../views/admin/adlogin', {
            title:"login page",
             layout:'../views/admin/adlogin'
        })
            } else {
                if (!bcrypt.compareSync(req.body.password, user.password)) {
                    res.render('../views/admin/adlogin', {
                        title:"login page",
                        layout:'../views/admin/adlogin'
                    })

                } else {
                    let payload = { subject: user._id }
                    let token = jwt.sign(payload, config.JWT_SECRET);

                    // res.render('./admin/index', {
                    //     title:"login page",
                    //     layout:'./admin/admin'
                    // })
                    res.redirect('/admin/dashboard')
                }

            }
        }
    })
}

exports.getAll=async (req,res)=>{
    const users=await User.find()
    res.status(200).json(users)
}