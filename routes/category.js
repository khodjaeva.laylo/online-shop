const router = require('express').Router()
const multer = require('multer');
const {eA,eAdmin,eOperator,eBoth} = require('../middleware/checkUser');
const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        // console.log(file);
        cb(null, './public/uploads/colors');
    },
    filename: function (req,file,cb) {
        // console.log(file);
        cb(null, `${Date.now()}-${file.originalname}`);
    }
});

const upload = multer({storage: storage});

const {addCategory,
    getALLCategory,
    deleteCategory,
    getColors,
    addColor,
    updateCategory,
    deleteColor,
    getById }=require('../controllers/categoryController')

router.get('/admin/addCategory', (req, res) => {
    res.render('./admin/addCategory', { title: 'category page', layout:'./admin/admin' })
})
router.get('/category', (req, res) => {
    res.render('./pages/category', { title: 'category page', layout:'./layout' })
})


// router.get('/deleteCategory.ejs', (req, res) => {
//     res.render('../views/admin/deleteCategory', { title: 'deleteCategory page', layout:'../views/admin/editCategory' })
// })

router.post('/admin/addcategory', addCategory)
router.get('/admin/category', getALLCategory)
router.post('/addcolor',/* upload.single('color'),*/ addColor);
router.get('/colorall', getColors)
router.delete('/admin/category/:id', deleteCategory)
router.delete('/color/:id', deleteColor)
router.patch('/admin/category/:id', updateCategory)
router.get('/admin/category/:id', getById)
module.exports=router