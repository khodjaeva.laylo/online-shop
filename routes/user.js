const express =require('express');
const router = express.Router();
const userController =require('../controllers/userController')
const csrf = require('csurf')
const csrfProtection = csrf({ cookie: true });

router.get('/register',csrfProtection,(req,res)=>{
    res.render('./pages/register',{title:'User Login page',layout:'./layout', csrfToken: req.csrfToken()})
})

router.get('/loginn',csrfProtection,(req,res)=>{
    res.render('./pages/login',{title:'User Login page',layout:'./layout', csrfToken: req.csrfToken()})
})

// router.get('/admin/users',(req,res)=>{
//     res.render('./admin/users',{title:'all users',layout:'./admin/admin'})
// })

router.post('/registerr',userController.register);
router.post('/loginn',userController.loginn);
router.get('/admin/users',userController.getAll)

module.exports=router;