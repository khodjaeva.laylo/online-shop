const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController')

//const cookieParser = require('cookie-parser')
const csrf = require('csurf')
const csrfProtection = csrf({ cookie: true });
//router.use(csrfProtection())

// app.post('/process', parseForm, csrfProtection, function (req, res) {
//     res.send('data is being processed')
// })


router.get('/adregister', csrfProtection,  (req, res) => {
    res.render('../views/admin/adregister', {
        title: 'register page',
        layout:'../views/admin/adregister' ,
     csrfToken: req.csrfToken()
    })
   // res.send(req.csrfToken())
   res.render('../views/admin/adlogin',csrfProtection, {
    title:"login page",
    layout:'../views/admin/adlogin',
    csrfToken: req.csrfToken()
})
})



router.post('/register', authController.register);
router.post('/login', authController.login);
router.get('/users', authController.getAll)
module.exports = router;