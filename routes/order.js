const router = require('express').Router()
const orderController = require('../controllers/orderController')

const {eA,eAdmin,eOperator,eBoth} = require('../middleware/checkUser');

// router.get('/order-history.ejs', (req, res) => {
//     res.render('../views/pages/order/order-history', { title: 'order-history page', layout:'../views/layout' })
// })
//
// router.get('/order-information.ejs', (req, res) => {
//     res.render('../views/pages/order/order-information', { title: 'order-information page', layout:'../views/layout' })
// })

router.get('/cart', (req, res) => {
    res.render('./pages/cart', { title: 'category page', layout:'./layout' })
})
router.get('/checkout', (req, res) => {
    res.render('./pages/checkout', { title: 'checkout page', layout:'./layout' })
})

router.post('/orders',orderController.addOrder);
router.get('/orders', orderController.getAllOrders);
router.get('/new-orders', orderController.getByNewOrder);
router.put('/order/:id',orderController.updateOrder);
router.delete('/order/:id',orderController.deleteOrder);
router.get('/orders/status', orderController.getStatus)
module.exports = router;
